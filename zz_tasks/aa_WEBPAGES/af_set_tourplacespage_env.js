var SCRIPTS_OUT_PATH                                     = 'ServerPart/public/01_SCRIPTS';
var SCRIPTS_IN_PATH                                    = '01_SCRIPTS/af_TOUR_PLACES_PAGE/**/*.*';
var SCRIPTS_OUT_FILE_NAME                              = 'af_tourplaces_page.js';


var SASS_OUT_PATH                                             = 'ServerPart/public/00_CSS';
var SASS_IN_PATH                                           = '00_SASS/af_TOUR_PLACES_PAGE/style.scss';
var SASS_OUT_FILE_NAME                                                = 'af_tourplaces_page.css';

var VIEW_T_IN_FRAG                              = 'ViewsT/ab_Fragments/af_TOUR_PLACES_PAGE/**/*.*';
var VIEW_T_OUT_BODY                        = 'ViewsT/aa_ServerPart/ab_body/af_TOUR_PLACES_PAGE';
 

module.exports = 
{
  
  tourplaces_page_mode: function () 
  {
  
                var options_script = {
                    SCRIPTS_OUT_PATH: SCRIPTS_OUT_PATH,
                    SCRIPTS_IN_PATH: SCRIPTS_IN_PATH,
                    SCRIPTS_OUT_FILE_NAME: SCRIPTS_OUT_FILE_NAME
                    
                };



                var options_sass = {
                    SASS_OUT_PATH: SASS_OUT_PATH,
                    SASS_IN_PATH: SASS_IN_PATH,
                    SASS_OUT_FILE_NAME: SASS_OUT_FILE_NAME
                    
                };


                var options_body = {
                    VIEW_T_OUT_BODY: VIEW_T_OUT_BODY,
                    VIEW_T_IN_FRAG: VIEW_T_IN_FRAG
                    
                };

                   return (javaScript(options_script) && SCSS(options_sass) && BODY_HTML(options_body) );

 

      
  }
  
  
  
};