

var gulp                                                      = require('gulp');
const template = require('gulp-template');
 
var concat                                             = require('gulp-concat');

 const fs = require('fs-extra');    


global.make_template = function (options)
{
    
                    var result =
                        gulp.src(  options.SCRIPTS_IN_PATH)
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.init()))
                        .pipe(concat(options.SCRIPTS_OUT_FILE_NAME))
                        .pipe(removeEmptyLines())


                        .pipe(gulpif(process.env.NODE_ENV === 'development',
                                prettify({
                                    mode: 'VERIFY_AND_WRITE'
                                }),
                                uglify()

                                ))
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.write()))
                        .pipe(gulp.dest(options.SCRIPTS_OUT_PATH ));
                  //      .pipe(notify({message: 'home page Javascript task complete'}));
                return result;


    
    //console.log("Test");
};


global.make_template = function (options)
{
    
   
     var result_scripts=gulp.src(options.SCRIPTS_IN+"/**/*.*")
        .pipe(gulp.dest(options.SCRIPTS_OUT));

    
     var result_sass=gulp.src(options.SASS_IN+"/**/*.*")
        .pipe(gulp.dest(options.SASS_OUT));

       
     var result_images=gulp.src(options.IMAGES_IN+"/**/*.*")
        .pipe(gulp.dest(options.IMAGES_OUT));



/// Views

     var result_workspace=gulp.src(options.WORKSPACE_IN+"/**/*.*")
             .pipe(template({NAME:options.NAME,DNAME:options.DNAME}))
             .pipe(gulp.dest(options.WORKSPACE_OUT));


     var result_fragment=gulp.src(options.FRAGMENTS_IN+"/**/*.*")
             .pipe(gulp.dest(options.FRAGMENTS_OUT));

//gulp
      var result_php=gulp.src(options.GULP_IN+"/**/*.*")
       .pipe(template({NAME:options.NAME,DNAME:options.DNAME}))
       .pipe(concat(options.NAME+"_set_env.js"))
             .pipe(gulp.dest(options.GULP_OUT));

 

//php Page
       var result_php=gulp.src(options.PHP_IN+"/**/*.*")
       .pipe(template({NAME:options.NAME,DNAME:options.DNAME}))
       .pipe(concat(options.NAME+"Controller.php"))
             .pipe(gulp.dest(options.PHP_OUT));

            
   
     
 
    return result_sass && result_scripts && result_images && result_workspace 
            && result_fragment && result_php && result_php;


    
    //console.log("Test");
};

global.remove_template = function (options)
{
    
  var result;                 
//Directory                
result=fs.remove(options.SASS_OUT, err => {
  if (err) return console.error(err)

 console.log('SASS removed');
});
result=fs.remove(options.SCRIPTS_OUT, err => {
  if (err) return console.error(err)
 console.log('SCRIPTS removed');
 // console.log('success!')
});
result=fs.remove(options.IMAGES_OUT, err => {
  if (err) return console.error(err)
 console.log('IMAGES removed');
 // console.log('success!')
});
result=fs.remove(options.WORKSPACE_OUT, err => {
  if (err) return console.error(err)
 console.log('WORKSPACE removed');
 // console.log('success!')
});
result=fs.remove(options.FRAGMENTS_OUT, err => {
  if (err) return console.error(err)
 console.log('FRAGMENTS removed');
 // console.log('success!')
});
                
///Files                 
result=fs.remove(options.GULP_OUT+"/"+options.NAME+"_set_env.js", err => {
  if (err) return console.error(err)

console.log(options.GULP_OUT+"/"+options.NAME+"_set_env.js"+' removed');
});

result=fs.remove(options.PHP_OUT+"/"+options.NAME+"Controller.php", err => {
  if (err) return console.error(err)

 console.log(options.PHP_OUT+"/"+options.NAME+"Controller.php"+' removed');
});

         
     
  return result;

    
    //console.log("Test");
};
