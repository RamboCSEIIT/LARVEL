<?php


use Phinx\Seed\AbstractSeed;

class GalleryProfile extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
         $faker = Faker\Factory::create();
         $data = [];
         for ($i = 0; $i < 10; $i++) 
         {
             
             
            
            
               
            $data[] = 
            [
               
           
                'image_link'    => "02_IMAGES/aa_HOME_PAGE/01_portfolio/portfolio_".$i.".jpg",
                'heading'       => $faker->country,
                'description'   => $faker->country

            ];

        }

         $this->insert('galleryProfile', $data);
       

    }
}
