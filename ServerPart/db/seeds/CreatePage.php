<?php


use Phinx\Seed\AbstractSeed;

class CreatePage extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $this->execute("
                        INSERT INTO `pages` VALUES (1,'About Wayanad Tours  and Travels','<p>Greetings from Wayanad dreams. Wayanad  Tours & Travels and Heritage Trekking are renowned for their personalized, top of the line services and reliability in the region. Our staff are experts in their field and some of them have been in the tourism field for ten years. They are also pioneers in guiding tourists to such exotic destinations like <strong> Banasura ,Muthanga,Edakkal caves </strong>etc </p>\n<p> Our major operations in <strong> Banasura,Meenmutty  water falls, Pookode lake,Kuruva islands,Edakkal caves and Lakkidi veiw point  </strong> include standard and budget tours, trekking, wildlife adventure, mountain biking  and special event tours. We offer meticulously planned itineraries to suit each and every tourist with diverse range of tastes, budget and schedule with an objective of having you with us on our trips. But if you have your own itinerary, please let us know. We assure you of our most competitive prices with utmost attention and best services at all times.</p>\n<p> We aim at promoting tourism that is environmental, cultural and traditional friendly and give utmost attention to the comforts and safety of our clients. To serve our clients better, we provide multi linguistic guides with air-conditioned car/jeep/coaches of different seating capacity. We have our own full fledged trekking company with high quality modern equipments and experienced Sherpa guides/staff. </p>\n\n<p>\nOur prestige lies in making your vacation success, full of fun, exciting and making it an experience that will add a lot to your life!! <br><br>“MAKES YOUR NEXT TRIP UNFORGETTABLE WITH US”<br><br><br>PRINCE DOMINIC <br>Chairman/Managing Director\n</p>','about-tour','2017-11-03 12:20:15',NULL,'about-tour-page'),(2,'Success','<h1>Success</h1><p>Welcome to Wayanad tours and travels!Check your mail for activation link</p>','success','2017-11-03 12:20:15',NULL,''),(3,'Not Found','<h1>Page Not Found!</h1><p>Page not found!</p>','page-not-found','2017-11-03 12:20:15',NULL,''),(4,'Account Activated','<h1>Acount Now Active</h1><p>Your account is now active, and you can log in.</p>','account-activated','2017-11-03 12:20:15',NULL,''),(5,'Saved','<h1>Enquiry Saved</h1><p>Your enquiry has been saved.</p>','enquiry-saved','2017-11-03 12:20:15',NULL,''),(6,'About package','<div class=\"well text-center\"><h2> One Night Two Days</h2><h3> First Day: </h3><p>6.30 AM AT Muthanga Wildlife sanctuary Edakkal Caves,Neelimala View Point,heritage museum,Pandum Rock overnight stay at hotel</p><h3> Second Day: </h3><p>Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake</p><h4>Total cost rs-5700 per couple </h4><h3>Important Note</h3><p>The 1 night 2 days package does not includeAny Air/Train fares,Lunch and dinner at Hotel,Personal Expense like camera films, mineral waters ,Portages,Tips and Gratitude and Activities of personal interest<br> </p></div><br><br><div class=\"well text-center\"><h2> Two Night Three Days</h2><h3> First Day: </h3><p>6.30 AM AT Muthanga Wildlife sanctuary Edakkal Caves,Neelimala View Point,heritage museum,Pandum Rock overnight stay at hotel</p><h3> Second Day: </h3><p>Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake Overnight Stay at Hotel.</p><h3>Third Day: </h3><p>Thirunelli Temble,Tholpetty Wildlife sanctuary.</p><h4>Total Cost Rs-8800 per couple </h4><h3>Important Note</h3><p>The 1 Night 2 DAYS package includes A/C Car Transportation( Indica),Sightseeing as per the itinerary,Daily BreakfastAll applicable Taxes<br> </p></div>','about-package','2017-11-03 16:13:57',NULL,'about-package-page');
        ");


    }
}
