<?php


use Phinx\Migration\AbstractMigration;

class GalleryPackage extends AbstractMigration
{
    public function up()
    {
         $this->execute("
             CREATE TABLE `galleryPackage` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `image_link` varchar(255) NOT NULL,
                                    `cost` int(11) NOT NULL DEFAULT 0,
                                    `tentative` int(11) NOT NULL DEFAULT 0,
                                    `make_front` int(11) NOT NULL DEFAULT 0,
                                    `payment_mode` varchar(255) NOT NULL,
                                    `day1`  text  DEFAULT NULL,
                                    `day2`  text  DEFAULT NULL,
                                    `day3`  text  DEFAULT NULL,
                                    `day4`  text  DEFAULT NULL,
                                    `day5`  text  DEFAULT NULL,                                    
                                    `heading`  varchar(1055) NOT NULL DEFAULT 'heading',
                                    `subheading`  varchar(1055) NOT NULL DEFAULT 'heading',
                                    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                    `updated_at` datetime DEFAULT NULL,
                                     PRIMARY KEY (`id`)
                                                                    ) 
        ");   

    }
}
