<?php


use Phinx\Migration\AbstractMigration;

class CreateFaq extends AbstractMigration
{
      //https://www.info.teradata.com/HTMLPubs/DB_TTU_16_00/index.html#page/Database_Management/B035-1094-160K/fti1472240591762.html  
     public function up()
    {
                 $this->execute("
                                    CREATE TABLE `Faq` (
                                                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                                                   `question` varchar(1055) NOT NULL,
                                                                   `answer` varchar(1055) NOT NULL,
                                                                   `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                                                   `updated_at` datetime DEFAULT NULL,
                                                                    PRIMARY KEY (`id`)
                                                         ) 
        ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        
          
        $this->execute(" 
            DROP TABLE Faq
         ");
    }

}
