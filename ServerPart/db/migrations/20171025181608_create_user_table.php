<?php


use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     
    public function change()
    {

    }*/
    
     /**
     * Migrate Up.
     */
    // wen u run
    public function up()
    {
       
 // create the table
    /*    
        $users = $this->table('users');
        $users->addColumn('first_name', 'string')
              ->addColumn('last_name', 'string')
              ->addColumn('email', 'string')  
              ->addColumn('password', 'string')
               ->addColumn('created_at', 'datetime',['default'=>'CURRENT_TIMESTAMP'])
              ->addColumn('updated_at', 'datetime',['null'=>true])  
              ->save();*/
        
         $this->execute("
                            CREATE TABLE `users` (
                                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                                    `first_name` varchar(255) NOT NULL,
                                                    `last_name` varchar(255) NOT NULL,
                                                    `email` varchar(255) NOT NULL,
                                                    `password` varchar(255) NOT NULL,
                                                    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                                    `updated_at` datetime DEFAULT NULL,
                                                    `active` int(11) DEFAULT '0',
                                                    `access_level` int(11) NOT NULL DEFAULT '1',
                                                    `mob_number`  VARCHAR(255) NOT NULL,
                                                     PRIMARY KEY (`id`),
                                                     UNIQUE KEY `email` (`email`)
)        ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
       // $this->dropTable('users');
        
        $this->execute(" 
            DROP TABLE users
         ");
    }
}
