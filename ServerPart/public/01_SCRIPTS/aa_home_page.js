  $('.owl-carousel').owlCarousel({
      // CSS Styles
      baseClass: "owl-carousel",
      theme: "owl-theme",
      loop: true,
      autoplay: true,
      margin: 10,
      responsiveClass: true,
      nav: true,
      rewind: true,
      responsive: {
          0: {
              items: 1,
              nav: true
          },
          600: {
              items: 2,
              nav: false
          },
          1000: {
              items: 3,
              nav: true,
              loop: false
          }
      }
  });
  $(window).scroll(function() {
      $(".slideanim").each(function() {
          var pos = $(this).offset().top;
          var winTop = $(window).scrollTop();
          if (pos < winTop + 600) {
              $(this).addClass("slide");
          }
      });
  });
  $(document).ready(function() {
      // Add smooth scrolling to all links in navbar + footer link
      $(".navbar a,#myFooter a[href='#package'],#myFooter a[href='#contact'],#myFooter a[href='#topnavid'], #myFooter a[href='#wayanad'], #myFooter a[href='#profile'] ").on('click', function(event) {
          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {
              // Prevent default anchor click behavior
              event.preventDefault();
              // Store hash
              var hash = this.hash;
              // Using jQuery's animate() method to add smooth page scroll
              // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
              $('html, body').animate({
                  scrollTop: $(hash).offset().top
              }, 900, function() {
                  // Add hash (#) to URL when done scrolling (default click behavior)
                  window.location.hash = hash;
              });
          } // End if
      });
  });
  $(document).ready(function() {
      function myMap() {
          var myCenter = new google.maps.LatLng(11.6700, 75.9578);
          var mapProp = {
              center: myCenter,
              zoom: 12,
              scrollwheel: false,
              draggable: false,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          var map = new google.maps.Map(document.getElementById("map"), mapProp);
          var marker = new google.maps.Marker({
              position: myCenter
          });
          marker.setMap(map);
      }
      google.maps.event.addDomListener(window, 'load', initMap, {
          passive: true
      });
  });

  function initMap() {
      // Create a new StyledMapType object, passing it an array of styles,
      // and the name to be displayed on the map type control.
      var styledMapType = new google.maps.StyledMapType(
          [{
                  elementType: 'geometry',
                  stylers: [{
                      color: '#ebe3cd'
                  }]
              },
              {
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#523735'
                  }]
              },
              {
                  elementType: 'labels.text.stroke',
                  stylers: [{
                      color: '#f5f1e6'
                  }]
              },
              {
                  featureType: 'administrative',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#c9b2a6'
                  }]
              },
              {
                  featureType: 'administrative.land_parcel',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#dcd2be'
                  }]
              },
              {
                  featureType: 'administrative.land_parcel',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#ae9e90'
                  }]
              },
              {
                  featureType: 'landscape.natural',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'poi',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'poi',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#93817c'
                  }]
              },
              {
                  featureType: 'poi.park',
                  elementType: 'geometry.fill',
                  stylers: [{
                      color: '#a5b076'
                  }]
              },
              {
                  featureType: 'poi.park',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#447530'
                  }]
              },
              {
                  featureType: 'road',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#f5f1e6'
                  }]
              },
              {
                  featureType: 'road.arterial',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#fdfcf8'
                  }]
              },
              {
                  featureType: 'road.highway',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#f8c967'
                  }]
              },
              {
                  featureType: 'road.highway',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#e9bc62'
                  }]
              },
              {
                  featureType: 'road.highway.controlled_access',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#e98d58'
                  }]
              },
              {
                  featureType: 'road.highway.controlled_access',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#db8555'
                  }]
              },
              {
                  featureType: 'road.local',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#806b63'
                  }]
              },
              {
                  featureType: 'transit.line',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'transit.line',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#8f7d77'
                  }]
              },
              {
                  featureType: 'transit.line',
                  elementType: 'labels.text.stroke',
                  stylers: [{
                      color: '#ebe3cd'
                  }]
              },
              {
                  featureType: 'transit.station',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'water',
                  elementType: 'geometry.fill',
                  stylers: [{
                      color: '#b9d3c2'
                  }]
              },
              {
                  featureType: 'water',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#92998d'
                  }]
              }
          ], {
              name: 'Styled Map'
          });
      // Create a map object, and include the MapTypeId to add
      // to the map type control.
      var map = new google.maps.Map(document.getElementById('map'), {
          center: {
              lat: 11.6700,
              lng: 75.9578
          },
          zoom: 11,
          mapTypeControlOptions: {
              mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                  'styled_map'
              ]
          }
      });
      //Associate the styled map with the MapTypeId and set it to display.
      map.mapTypes.set('styled_map', styledMapType);
      map.setMapTypeId('styled_map');
      var myCenter = new google.maps.LatLng(11.6700, 75.9578);
      var marker = new google.maps.Marker({
          position: myCenter
      });
      marker.setMap(map);
  }
  $("#sub_nav1").on('click', function() {
      var id = $(this).attr('id');
      // alert(id);
      if (id != "navbarDropdownMenuLink")
          $('.navbar-collapse').collapse('hide');
  });
  $("#sub_nav2").on('click', function() {
      var id = $(this).attr('id');
      // alert(id);
      if (id != "navbarDropdownMenuLink")
          $('.navbar-collapse').collapse('hide');
  });
  $("#sub_nav3").on('click', function() {
      var id = $(this).attr('id');
      // alert(id);
      if (id != "navbarDropdownMenuLink")
          $('.navbar-collapse').collapse('hide');
  });
  $('.navbar-nav>li>a').on('click', function() {
      var id = $(this).attr('id');
      // alert(id);
      if (id != "navbarDropdownMenuLink")
          $('.navbar-collapse').collapse('hide');
  });
  $('.navbar-collapse').on('shown.bs.collapse', function() {
      //alert("hello");
      $('#tag').html("Menu");
      $('#tag').removeClass("fa fa-bars");
      $('#tag').addClass("fa fa-times");
  });
  $('.navbar-collapse').on('hidden.bs.collapse', function() {
      // $('#tag').html("O");
      $('#tag').html("Menu");
      $('#tag').removeClass("fa fa-times");
      $('#tag').addClass("fa fa-bars");
  });
  // magic.js
  $(document).ready(function() {
      // process the form
      $('#formsub').submit(function(event) {
          // get the form data
          // there are many ways to get this data using jQuery (you can use the class or id also)
          var formData = {
              //   'name': $('input[name=name]').val(),
              'email': $('input[name=email]').val(),
              //  'superheroAlias': $('input[name=superheroAlias]').val()
          };
          // process the form
          $.ajax({
                  type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url: '/subs', // the url where we want to POST
                  data: formData, // our data object
                  dataType: 'json', // what type of data do we expect back from the server
                  encode: true
              })
              // using the done promise callback
              .done(function(data) {
                  // log data to the console so we can see
                  console.log(data);
                  // here we will handle errors and validation messages
                  if (!data.success) {
                      $("#submsg").html(data.errors.email);
                      /*                        // handle errors for name ---------------
                                              if (data.errors.name) {
                                                  $('#name-group').addClass('has-error'); // add the error class to show red input
                                                  $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                                              }
                                              // handle errors for email ---------------
                                              if (data.errors.email) {
                                                  $('#email-group').addClass('has-error'); // add the error class to show red input
                                                  $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                                              }
                                              // handle errors for superhero alias ---------------
                                              if (data.errors.superheroAlias) {
                                                  $('#superhero-group').addClass('has-error'); // add the error class to show red input
                                                  $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                                              }*/
                  } else {
                      // ALL GOOD! just show the success message!
                      // $('form').append('<div class="alert alert-success">' + data.message + '</div>');
                      // usually after form submission, you'll want to redirect
                      // window.location = '/thank-you'; // redirect a user to another page
                      //  alert('success'+data.message); // for now we'll just alert the user
                      $("#submsg").html(data.message);
                  }
              });
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
      });
  });
  // magic.js
  $(document).ready(function() {
      // process the form
      $('#contactId').submit(function(event) {
          // get the form data
          // there are many ways to get this data using jQuery (you can use the class or id also)
          var formData = {
              'ame': $('input[name=name]').val(),
              'email': $('input[name=email]').val(),
              'comments': $('input[name=comments]').val()
          };
          // process the form
          $.ajax({
                  type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url: '/contact', // the url where we want to POST
                  data: formData, // our data object
                  dataType: 'json', // what type of data do we expect back from the server
                  encode: true
              })
              // using the done promise callback
              .done(function(data) {
                  // log data to the console so we can see
                  console.log(data);
                  // here we will handle errors and validation messages
                  if (!data.success) {
                      $("#contactmsg").html(data.errors.email);
                      /*                        // handle errors for name ---------------
                       if (data.errors.name) {
                       $('#name-group').addClass('has-error'); // add the error class to show red input
                       $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for email ---------------
                       if (data.errors.email) {
                       $('#email-group').addClass('has-error'); // add the error class to show red input
                       $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for superhero alias ---------------
                       if (data.errors.superheroAlias) {
                       $('#superhero-group').addClass('has-error'); // add the error class to show red input
                       $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                       }*/
                  } else {
                      // ALL GOOD! just show the success message!
                      // $('form').append('<div class="alert alert-success">' + data.message + '</div>');
                      // usually after form submission, you'll want to redirect
                      // window.location = '/thank-you'; // redirect a user to another page
                      //  alert('success'+data.message); // for now we'll just alert the user
                      $("#contactmsg").html(data.message);
                  }
              });
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
      });
      $('#enquiryform').submit(function(event) {
          //  var result = $('#enqid').val();
          var name = $('#name').val();
          var email = $('#email').val();
          var mobile = $('#mobile').val();
          var comments = $('#comments').val();
          // alert("hai");
          // get the form data
          // there are many ways to get this data using jQuery (you can use the class or id also)
          var formData = {
              'name': name,
              'email': email,
              'comments': comments,
              'mobile': mobile
          };
          // console.log("Hello :: "+formData);
          // 
          // 
          // process the form
          // alert(formData.name+"::"+formData.email +"::"+formData.mobile+"::"+formData.comments);
          $.ajax({
                  type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url: '/contact', // the url where we want to POST
                  data: formData, // our data object
                  dataType: 'json', // what type of data do we expect back from the server
                  encode: true
              })
              // using the done promise callback
              .done(function(data) {
                  // log data to the console so we can see
                  console.log(data);
                  // here we will handle errors and validation messages
                  if (!data.success) {
                      $("#contactmsg").html(data.message);
                      /*                        // handle errors for name ---------------
                       if (data.errors.name) {
                       $('#name-group').addClass('has-error'); // add the error class to show red input
                       $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for email ---------------
                       if (data.errors.email) {
                       $('#email-group').addClass('has-error'); // add the error class to show red input
                       $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for superhero alias ---------------
                       if (data.errors.superheroAlias) {
                       $('#superhero-group').addClass('has-error'); // add the error class to show red input
                       $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                       }*/
                  } else {
                      // ALL GOOD! just show the success message!
                      // $('form').append('<div class="alert alert-success">' + data.message + '</div>');
                      // usually after form submission, you'll want to redirect
                      // window.location = '/thank-you'; // redirect a user to another page
                      //  alert('success'+data.message); // for now we'll just alert the user
                      $("#contactmsg").html(data.message);
                  }
              });
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
      });
  });
  $(document).ready(function() {
      // get location from user's IP address
      var latitude = 11.6700;
      var longitude = 75.9578;
      $("#location").html(
          "Location :  Wayanad "
      );
      Date.prototype.yyyymmdd = function() {
          var yyyy = this.getFullYear().toString();
          var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
          var dd = this.getDate().toString();
          return yyyy + "/" + (mm[1] ? mm : "0" + mm[0]) + "/" + (dd[1] ? dd : "0" + dd[0]); // padding
      };
      var date = new Date();
      $("#date").html(date);
      // get weather using OpenWeatherMap API
      $.getJSON(
          "https://cors.5apps.com/?uri=http://api.openweathermap.org/data/2.5/weather?lat=" +
          latitude +
          "&lon=" +
          longitude +
          "&units=metric&APPID=c3e00c8860695fd6096fe32896042eda",
          function(data) {
              var windSpeedkmh = Math.round(data.wind.speed * 3.6);
              var Celsius = Math.round(data.main.temp);
              var iconId = data.weather[0].icon;
              var weatherURL = "http://openweathermap.org/img/w/" + iconId + ".png";
              var iconImg = "<img src='" + weatherURL + "'>";
              // $("#sky-image").html(iconImg);
              $("#weather-id").html("Skies: " + data.weather[0].description);
              $("#temperature").html(Celsius);
              $("#toFahrenheit").click(function() {
                  $("#temperature").html(Math.round(9 / 5 * Celsius + 32));
                  $("#wind-speed").html(Math.round(windSpeedkmh * 0.621) + " mph");
              });
              $("#toCelsius").click(function() {
                  $("#temperature").html(Celsius);
                  $("#wind-speed").html(windSpeedkmh + " km/hr");
              });
              $("#wind-speed").html(windSpeedkmh + " km/h");
              $("#humidity").html(data.main.humidity + " %");
          }
      );
      /*
      $.getJSON("https://ipinfo.io", function(info) {
        var locString = info.loc.split(", ");
        var latitude = parseFloat(locString[0]);
        var longitude = parseFloat(locString[1]);
        $("#location").html(
          "Location: " + info.city + ", " + info.region + ", " + info.country
        );
        // get weather using OpenWeatherMap API
        $.getJSON(
          "https://cors.5apps.com/?uri=http://api.openweathermap.org/data/2.5/weather?lat=" +
            latitude +
            "&lon=" +
            longitude +
            "&units=metric&APPID=c3e00c8860695fd6096fe32896042eda",
          function(data) {
            var windSpeedkmh = Math.round(data.wind.speed * 3.6);
            var Celsius = Math.round(data.main.temp);
            var iconId = data.weather[0].icon;
            var weatherURL = "http://openweathermap.org/img/w/" + iconId + ".png";
            var iconImg = "<img src='" + weatherURL + "'>";
            $("#sky-image").html(iconImg);
            $("#weather-id").html("Skies: " + data.weather[0].description);
            $("#temperature").html(Celsius);
            $("#toFahrenheit").click(function() {
              $("#temperature").html(Math.round(9 / 5 * Celsius + 32));
              $("#wind-speed").html(Math.round(windSpeedkmh * 0.621) + " mph");
            });
            $("#toCelsius").click(function() {
              $("#temperature").html(Celsius);
              $("#wind-speed").html(windSpeedkmh + " km/hr");
            });
            $("#wind-speed").html(windSpeedkmh + " km/h");
            $("#humidity").html("Humidity: " + data.main.humidity + " %");
          }
        );
      });*/
  });
  $(document).ready(function() {
      hide_cube(true);
  });

  function hide_cube(hide) {
      if (hide == true) {
          $("#wayanad").css("visibility", "hidden");
      } else {
          $("#wayanad").css("visibility", "visible");
      }
  }
  /*
   $(window).load(function() { 
       //insert all your ajax callback code here. 
       //Which will run only after page is fully loaded in background.
       // 
    });
  */
  $(window).on("load", function() {
      // Things that need to happen after full load
      hide_cube(false);
  });
  $(document).ready(function() {
      $("#button_pkg").click(function(e) {
          //  alert("The paragraph was clicked.");
          e.preventDefault();
          //reset animation
          e.target.classList.remove('animate');
          e.target.classList.add('animate');
          setTimeout(function() {
              e.target.classList.remove('animate');
              window.location.href = "/package";
          }, 700);
      });
      var animateButton = function(e) {
          e.preventDefault();
          //reset animation
          e.target.classList.remove('animate');
          e.target.classList.add('animate');
          setTimeout(function() {
              e.target.classList.remove('animate');
              window.location.href = "/package";
          }, 700);
      };
      /*
          var bubblyButtons = document.getElementsByClassName("bubbly-button");
          for (var i = 0; i < bubblyButtons.length; i++) {
              bubblyButtons[i].addEventListener('click', animateButton, false);
          }
      */
  });
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX293bF9jYXJvdXNlbC9vd2xfY2Fyb3VzZWwuanMiLCJhYl9TbGlkZV9Gcm9tX0JvdHRvbS9zbGlkZV9ib3R0b20uanMiLCJhY19tb3ZlX2FuaW0vbW92ZV9hbmltLmpzIiwiYWRfZ29vZ2xlX21hcC9nb29nbGUtbWFwLmpzIiwiYWVfbW9iaWxlX21lbnUvbWVudV9tb2JpbGUuanMiLCJhZl9zdWJzY3JpcHRpb24vc3Vic2NyaXB0aW9uLmpzIiwiYWdfY29udGFjdC9jb250YWN0LmpzIiwiYWhfd2F5YW5hZC93YXlhbmFkLmpzIiwiYWlfcGFja2FnZS9wYWNrYWdlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNoQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNyS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2pEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDdEVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3ZRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3ZDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiYWFfaG9tZV9wYWdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiICBcbiAgJCgnLm93bC1jYXJvdXNlbCcpLm93bENhcm91c2VsKHtcbiAgXG4gICAgIC8vIENTUyBTdHlsZXNcbiAgICBiYXNlQ2xhc3MgOiBcIm93bC1jYXJvdXNlbFwiLFxuICAgIHRoZW1lIDogXCJvd2wtdGhlbWVcIixcbiAgICBsb29wOnRydWUsXG4gICAgYXV0b3BsYXk6dHJ1ZSwgICAgICAgIFxuICAgIG1hcmdpbjoxMCxcbiAgICByZXNwb25zaXZlQ2xhc3M6dHJ1ZSxcbiAgICBuYXY6dHJ1ZSxcbiAgICByZXdpbmQ6dHJ1ZSxcbiAgICByZXNwb25zaXZlOntcbiAgICAgICAgMDp7XG4gICAgICAgICAgICBpdGVtczoxLFxuICAgICAgICAgICAgbmF2OnRydWVcbiAgICAgICAgfSxcbiAgICAgICAgNjAwOntcbiAgICAgICAgICAgIGl0ZW1zOjIsXG4gICAgICAgICAgICBuYXY6ZmFsc2VcbiAgICAgICAgfSxcbiAgICAgICAgMTAwMDp7XG4gICAgICAgICAgICBpdGVtczozLFxuICAgICAgICAgICAgbmF2OnRydWUsXG4gICAgICAgICAgICBsb29wOmZhbHNlXG4gICAgICAgIH1cbiAgICB9XG59KTtcblxuIiwiICAkKHdpbmRvdykuc2Nyb2xsKGZ1bmN0aW9uKCkge1xuICAkKFwiLnNsaWRlYW5pbVwiKS5lYWNoKGZ1bmN0aW9uKCl7XG4gICAgdmFyIHBvcyA9ICQodGhpcykub2Zmc2V0KCkudG9wO1xuXG4gICAgdmFyIHdpblRvcCA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcbiAgICBpZiAocG9zIDwgd2luVG9wICsgNjAwKSB7XG4gICAgICAkKHRoaXMpLmFkZENsYXNzKFwic2xpZGVcIik7XG4gICAgfVxuICB9KTtcbn0pOyAgICAgICAgIFxuIFxuIiwiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKClcbntcbiAgICAvLyBBZGQgc21vb3RoIHNjcm9sbGluZyB0byBhbGwgbGlua3MgaW4gbmF2YmFyICsgZm9vdGVyIGxpbmtcbiAgICAkKFwiLm5hdmJhciBhLCNteUZvb3RlciBhW2hyZWY9JyNwYWNrYWdlJ10sI215Rm9vdGVyIGFbaHJlZj0nI2NvbnRhY3QnXSwjbXlGb290ZXIgYVtocmVmPScjdG9wbmF2aWQnXSwgI215Rm9vdGVyIGFbaHJlZj0nI3dheWFuYWQnXSwgI215Rm9vdGVyIGFbaHJlZj0nI3Byb2ZpbGUnXSBcIikub24oJ2NsaWNrJywgZnVuY3Rpb24gKGV2ZW50KSB7XG5cbiAgICAgICAgLy8gTWFrZSBzdXJlIHRoaXMuaGFzaCBoYXMgYSB2YWx1ZSBiZWZvcmUgb3ZlcnJpZGluZyBkZWZhdWx0IGJlaGF2aW9yXG4gICAgICAgIGlmICh0aGlzLmhhc2ggIT09IFwiXCIpXG4gICAgICAgIHtcblxuICAgICAgICAgICAgLy8gUHJldmVudCBkZWZhdWx0IGFuY2hvciBjbGljayBiZWhhdmlvclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgLy8gU3RvcmUgaGFzaFxuICAgICAgICAgICAgdmFyIGhhc2ggPSB0aGlzLmhhc2g7XG5cbiAgICAgICAgICAgIC8vIFVzaW5nIGpRdWVyeSdzIGFuaW1hdGUoKSBtZXRob2QgdG8gYWRkIHNtb290aCBwYWdlIHNjcm9sbFxuICAgICAgICAgICAgLy8gVGhlIG9wdGlvbmFsIG51bWJlciAoOTAwKSBzcGVjaWZpZXMgdGhlIG51bWJlciBvZiBtaWxsaXNlY29uZHMgaXQgdGFrZXMgdG8gc2Nyb2xsIHRvIHRoZSBzcGVjaWZpZWQgYXJlYVxuICAgICAgICAgICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgIHNjcm9sbFRvcDogJChoYXNoKS5vZmZzZXQoKS50b3BcbiAgICAgICAgICAgIH0sIDkwMCwgZnVuY3Rpb24gKCkge1xuXG4gICAgICAgICAgICAgICAgLy8gQWRkIGhhc2ggKCMpIHRvIFVSTCB3aGVuIGRvbmUgc2Nyb2xsaW5nIChkZWZhdWx0IGNsaWNrIGJlaGF2aW9yKVxuICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5oYXNoID0gaGFzaDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9IC8vIEVuZCBpZlxuICAgIH0pO1xuXG5cblxuXG5cblxufSk7IiwiXG4gXG5cblxuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKVxue1xuXG5cbiAgICBmdW5jdGlvbiBteU1hcCgpIHtcblxuICAgICAgICB2YXIgbXlDZW50ZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKDExLjY3MDAsIDc1Ljk1NzgpO1xuICAgICAgICB2YXIgbWFwUHJvcCA9IHtjZW50ZXI6IG15Q2VudGVyLCB6b29tOiAxMiwgc2Nyb2xsd2hlZWw6IGZhbHNlLCBkcmFnZ2FibGU6IGZhbHNlLCBtYXBUeXBlSWQ6IGdvb2dsZS5tYXBzLk1hcFR5cGVJZC5ST0FETUFQfTtcbiAgICAgICAgdmFyIG1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJtYXBcIiksIG1hcFByb3ApO1xuICAgICAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7cG9zaXRpb246IG15Q2VudGVyfSk7XG4gICAgICAgIG1hcmtlci5zZXRNYXAobWFwKTtcbiAgICB9XG5cbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGREb21MaXN0ZW5lcih3aW5kb3csICdsb2FkJywgaW5pdE1hcCwge3Bhc3NpdmU6IHRydWV9KTtcblxuXG5cblxufSk7XG5cbiBcbmZ1bmN0aW9uIGluaXRNYXAoKSB7XG5cbiAgICAvLyBDcmVhdGUgYSBuZXcgU3R5bGVkTWFwVHlwZSBvYmplY3QsIHBhc3NpbmcgaXQgYW4gYXJyYXkgb2Ygc3R5bGVzLFxuICAgIC8vIGFuZCB0aGUgbmFtZSB0byBiZSBkaXNwbGF5ZWQgb24gdGhlIG1hcCB0eXBlIGNvbnRyb2wuXG4gICAgdmFyIHN0eWxlZE1hcFR5cGUgPSBuZXcgZ29vZ2xlLm1hcHMuU3R5bGVkTWFwVHlwZShcbiAgICAgICAgICAgIFtcbiAgICAgICAgICAgICAgICB7ZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsIHN0eWxlcnM6IFt7Y29sb3I6ICcjZWJlM2NkJ31dfSxcbiAgICAgICAgICAgICAgICB7ZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5maWxsJywgc3R5bGVyczogW3tjb2xvcjogJyM1MjM3MzUnfV19LFxuICAgICAgICAgICAgICAgIHtlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LnN0cm9rZScsIHN0eWxlcnM6IFt7Y29sb3I6ICcjZjVmMWU2J31dfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnYWRtaW5pc3RyYXRpdmUnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5LnN0cm9rZScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYzliMmE2J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnYWRtaW5pc3RyYXRpdmUubGFuZF9wYXJjZWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5LnN0cm9rZScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZGNkMmJlJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnYWRtaW5pc3RyYXRpdmUubGFuZF9wYXJjZWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2FlOWU5MCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ2xhbmRzY2FwZS5uYXR1cmFsJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZGZkMmFlJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncG9pJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZGZkMmFlJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncG9pJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyM5MzgxN2MnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2kucGFyaycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYTViMDc2J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncG9pLnBhcmsnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzQ0NzUzMCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNmNWYxZTYnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmFydGVyaWFsJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZmRmY2Y4J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5JyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZjhjOTY3J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5JyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeS5zdHJva2UnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2U5YmM2Mid9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQuaGlnaHdheS5jb250cm9sbGVkX2FjY2VzcycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnknLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2U5OGQ1OCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQuaGlnaHdheS5jb250cm9sbGVkX2FjY2VzcycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkYjg1NTUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmxvY2FsJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyM4MDZiNjMnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd0cmFuc2l0LmxpbmUnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd0cmFuc2l0LmxpbmUnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzhmN2Q3Nyd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3RyYW5zaXQubGluZScsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNlYmUzY2QnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd0cmFuc2l0LnN0YXRpb24nLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd3YXRlcicsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYjlkM2MyJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnd2F0ZXInLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzkyOTk4ZCd9XVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICB7bmFtZTogJ1N0eWxlZCBNYXAnfSk7XG5cbiAgICAvLyBDcmVhdGUgYSBtYXAgb2JqZWN0LCBhbmQgaW5jbHVkZSB0aGUgTWFwVHlwZUlkIHRvIGFkZFxuICAgIC8vIHRvIHRoZSBtYXAgdHlwZSBjb250cm9sLlxuICAgIHZhciBtYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtYXAnKSwge1xuICAgICAgICBjZW50ZXI6IHtsYXQ6IDExLjY3MDAsIGxuZzogNzUuOTU3OH0sXG4gICAgICAgIHpvb206IDExLFxuICAgICAgICBtYXBUeXBlQ29udHJvbE9wdGlvbnM6IHtcbiAgICAgICAgICAgIG1hcFR5cGVJZHM6IFsncm9hZG1hcCcsICdzYXRlbGxpdGUnLCAnaHlicmlkJywgJ3RlcnJhaW4nLFxuICAgICAgICAgICAgICAgICdzdHlsZWRfbWFwJ11cbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgLy9Bc3NvY2lhdGUgdGhlIHN0eWxlZCBtYXAgd2l0aCB0aGUgTWFwVHlwZUlkIGFuZCBzZXQgaXQgdG8gZGlzcGxheS5cbiAgICBtYXAubWFwVHlwZXMuc2V0KCdzdHlsZWRfbWFwJywgc3R5bGVkTWFwVHlwZSk7XG4gICAgbWFwLnNldE1hcFR5cGVJZCgnc3R5bGVkX21hcCcpO1xuXG4gICAgdmFyIG15Q2VudGVyID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZygxMS42NzAwLCA3NS45NTc4KTtcbiAgICB2YXIgbWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7cG9zaXRpb246IG15Q2VudGVyfSk7XG4gICAgbWFya2VyLnNldE1hcChtYXApO1xuICAgIFxuICAgICBcblxuICAgIFxufSIsIiAgICAkKFwiI3N1Yl9uYXYxXCIpLm9uKCdjbGljaycsIGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAgICAgIHZhciBpZCA9ICQodGhpcykuYXR0cignaWQnKTtcbiAgICAgICAgICAgLy8gYWxlcnQoaWQpO1xuICAgICAgICAgICBpZihpZCAhPSBcIm5hdmJhckRyb3Bkb3duTWVudUxpbmtcIilcbiAgICAgICAgICAgICAgICAgICQoJy5uYXZiYXItY29sbGFwc2UnKS5jb2xsYXBzZSgnaGlkZScpO1xuICAgIH0pO1xuICAgIFxuICAgJChcIiNzdWJfbmF2MlwiKS5vbignY2xpY2snLCBmdW5jdGlvbigpXG4gICAge1xuICAgICAgICAgICB2YXIgaWQgPSAkKHRoaXMpLmF0dHIoJ2lkJyk7XG4gICAgICAgICAgIC8vIGFsZXJ0KGlkKTtcbiAgICAgICAgICAgaWYoaWQgIT0gXCJuYXZiYXJEcm9wZG93bk1lbnVMaW5rXCIpXG4gICAgICAgICAgICAgICAgICAkKCcubmF2YmFyLWNvbGxhcHNlJykuY29sbGFwc2UoJ2hpZGUnKTtcbiAgICB9KTtcbiAgICAkKFwiI3N1Yl9uYXYzXCIpLm9uKCdjbGljaycsIGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAgICAgIHZhciBpZCA9ICQodGhpcykuYXR0cignaWQnKTtcbiAgICAgICAgICAgLy8gYWxlcnQoaWQpO1xuICAgICAgICAgICBpZihpZCAhPSBcIm5hdmJhckRyb3Bkb3duTWVudUxpbmtcIilcbiAgICAgICAgICAgICAgICAgICQoJy5uYXZiYXItY29sbGFwc2UnKS5jb2xsYXBzZSgnaGlkZScpO1xuICAgIH0pO1xuICAgICBcbiAgICAgJCgnLm5hdmJhci1uYXY+bGk+YScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAgICAgIHZhciBpZCA9ICQodGhpcykuYXR0cignaWQnKTtcbiAgICAgICAgICAgLy8gYWxlcnQoaWQpO1xuICAgICAgICAgICBpZihpZCAhPSBcIm5hdmJhckRyb3Bkb3duTWVudUxpbmtcIilcbiAgICAgICAgICAgICAgICAgICQoJy5uYXZiYXItY29sbGFwc2UnKS5jb2xsYXBzZSgnaGlkZScpO1xuICAgIH0pO1xuICAgXG4gICAgXG4gICAgJCgnLm5hdmJhci1jb2xsYXBzZScpLm9uKCdzaG93bi5icy5jb2xsYXBzZScsIGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAgLy9hbGVydChcImhlbGxvXCIpO1xuICAgICAgICAkKCcjdGFnJykuaHRtbChcIk1lbnVcIik7XG4gICAgICAgICQoJyN0YWcnKS5yZW1vdmVDbGFzcyhcImZhIGZhLWJhcnNcIik7XG4gICAgICAgICQoJyN0YWcnKS5hZGRDbGFzcyhcImZhIGZhLXRpbWVzXCIpO1xuICAgICAgICBcbiAgICAgICBcbiAgICB9KTtcbiAgICBcbiAgICBcbiAgICAkKCcubmF2YmFyLWNvbGxhcHNlJykub24oJ2hpZGRlbi5icy5jb2xsYXBzZScsIGZ1bmN0aW9uKClcbiAgICB7XG4gICAgICAvLyAkKCcjdGFnJykuaHRtbChcIk9cIik7XG4gICAgICAgICAgICAgJCgnI3RhZycpLmh0bWwoXCJNZW51XCIpO1xuICAgICAgICAgICAgJCgnI3RhZycpLnJlbW92ZUNsYXNzKFwiZmEgZmEtdGltZXNcIik7XG4gICAgICAgICAgICAgICQoJyN0YWcnKS5hZGRDbGFzcyhcImZhIGZhLWJhcnNcIik7XG4gICAgfSk7IiwiLy8gbWFnaWMuanNcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAkKCcjZm9ybXN1YicpLnN1Ym1pdChmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBnZXQgdGhlIGZvcm0gZGF0YVxuICAgICAgICAvLyB0aGVyZSBhcmUgbWFueSB3YXlzIHRvIGdldCB0aGlzIGRhdGEgdXNpbmcgalF1ZXJ5ICh5b3UgY2FuIHVzZSB0aGUgY2xhc3Mgb3IgaWQgYWxzbylcbiAgICAgICAgdmFyIGZvcm1EYXRhID0ge1xuICAgICAgICAgLy8gICAnbmFtZSc6ICQoJ2lucHV0W25hbWU9bmFtZV0nKS52YWwoKSxcbiAgICAgICAgICAgICdlbWFpbCc6ICQoJ2lucHV0W25hbWU9ZW1haWxdJykudmFsKCksXG4gICAgICAgICAgLy8gICdzdXBlcmhlcm9BbGlhcyc6ICQoJ2lucHV0W25hbWU9c3VwZXJoZXJvQWxpYXNdJykudmFsKClcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBwcm9jZXNzIHRoZSBmb3JtXG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICB0eXBlOiAnUE9TVCcsIC8vIGRlZmluZSB0aGUgdHlwZSBvZiBIVFRQIHZlcmIgd2Ugd2FudCB0byB1c2UgKFBPU1QgZm9yIG91ciBmb3JtKVxuICAgICAgICAgICAgdXJsOiAnL3N1YnMnLCAvLyB0aGUgdXJsIHdoZXJlIHdlIHdhbnQgdG8gUE9TVFxuICAgICAgICAgICAgZGF0YTogZm9ybURhdGEsIC8vIG91ciBkYXRhIG9iamVjdFxuICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJywgLy8gd2hhdCB0eXBlIG9mIGRhdGEgZG8gd2UgZXhwZWN0IGJhY2sgZnJvbSB0aGUgc2VydmVyXG4gICAgICAgICAgICBlbmNvZGU6IHRydWVcbiAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAvLyB1c2luZyB0aGUgZG9uZSBwcm9taXNlIGNhbGxiYWNrXG4gICAgICAgICAgICAgICAgLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyBsb2cgZGF0YSB0byB0aGUgY29uc29sZSBzbyB3ZSBjYW4gc2VlXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGhlcmUgd2Ugd2lsbCBoYW5kbGUgZXJyb3JzIGFuZCB2YWxpZGF0aW9uIG1lc3NhZ2VzXG4gICAgICAgICAgICAgICAgICAgIGlmICghZGF0YS5zdWNjZXNzKSBcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjc3VibXNnXCIpLmh0bWwoZGF0YS5lcnJvcnMuZW1haWwpO1xuLyogICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBuYW1lIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjbmFtZS1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5uYW1lICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBlbWFpbCAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5lbWFpbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuZW1haWwgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIHN1cGVyaGVybyBhbGlhcyAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcyArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFMTCBHT09EISBqdXN0IHNob3cgdGhlIHN1Y2Nlc3MgbWVzc2FnZSFcbiAgICAgICAgICAgICAgICAgICAgICAgLy8gJCgnZm9ybScpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LXN1Y2Nlc3NcIj4nICsgZGF0YS5tZXNzYWdlICsgJzwvZGl2PicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c3VhbGx5IGFmdGVyIGZvcm0gc3VibWlzc2lvbiwgeW91J2xsIHdhbnQgdG8gcmVkaXJlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbmRvdy5sb2NhdGlvbiA9ICcvdGhhbmsteW91JzsgLy8gcmVkaXJlY3QgYSB1c2VyIHRvIGFub3RoZXIgcGFnZVxuICAgICAgICAgICAgICAgICAgICAgIC8vICBhbGVydCgnc3VjY2VzcycrZGF0YS5tZXNzYWdlKTsgLy8gZm9yIG5vdyB3ZSdsbCBqdXN0IGFsZXJ0IHRoZSB1c2VyXG4gICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgJChcIiNzdWJtc2dcIikuaHRtbChkYXRhLm1lc3NhZ2UpO1xuXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIC8vIHN0b3AgdGhlIGZvcm0gZnJvbSBzdWJtaXR0aW5nIHRoZSBub3JtYWwgd2F5IGFuZCByZWZyZXNoaW5nIHRoZSBwYWdlXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgfSk7XG5cbn0pOyIsIi8vIG1hZ2ljLmpzXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG5cbiAgICAvLyBwcm9jZXNzIHRoZSBmb3JtXG4gICAgJCgnI2NvbnRhY3RJZCcpLnN1Ym1pdChmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBnZXQgdGhlIGZvcm0gZGF0YVxuICAgICAgICAvLyB0aGVyZSBhcmUgbWFueSB3YXlzIHRvIGdldCB0aGlzIGRhdGEgdXNpbmcgalF1ZXJ5ICh5b3UgY2FuIHVzZSB0aGUgY2xhc3Mgb3IgaWQgYWxzbylcbiAgICAgICAgdmFyIGZvcm1EYXRhID0ge1xuICAgICAgICAgICAgJ2FtZSc6ICQoJ2lucHV0W25hbWU9bmFtZV0nKS52YWwoKSxcbiAgICAgICAgICAgICdlbWFpbCc6ICQoJ2lucHV0W25hbWU9ZW1haWxdJykudmFsKCksXG4gICAgICAgICAgICAnY29tbWVudHMnOiAkKCdpbnB1dFtuYW1lPWNvbW1lbnRzXScpLnZhbCgpXG4gICAgICAgIH07XG5cbiAgICAgICAgLy8gcHJvY2VzcyB0aGUgZm9ybVxuICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdHlwZTogJ1BPU1QnLCAvLyBkZWZpbmUgdGhlIHR5cGUgb2YgSFRUUCB2ZXJiIHdlIHdhbnQgdG8gdXNlIChQT1NUIGZvciBvdXIgZm9ybSlcbiAgICAgICAgICAgIHVybDogJy9jb250YWN0JywgLy8gdGhlIHVybCB3aGVyZSB3ZSB3YW50IHRvIFBPU1RcbiAgICAgICAgICAgIGRhdGE6IGZvcm1EYXRhLCAvLyBvdXIgZGF0YSBvYmplY3RcbiAgICAgICAgICAgIGRhdGFUeXBlOiAnanNvbicsIC8vIHdoYXQgdHlwZSBvZiBkYXRhIGRvIHdlIGV4cGVjdCBiYWNrIGZyb20gdGhlIHNlcnZlclxuICAgICAgICAgICAgZW5jb2RlOiB0cnVlXG4gICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLy8gdXNpbmcgdGhlIGRvbmUgcHJvbWlzZSBjYWxsYmFja1xuICAgICAgICAgICAgICAgIC5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gbG9nIGRhdGEgdG8gdGhlIGNvbnNvbGUgc28gd2UgY2FuIHNlZVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBoZXJlIHdlIHdpbGwgaGFuZGxlIGVycm9ycyBhbmQgdmFsaWRhdGlvbiBtZXNzYWdlc1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWRhdGEuc3VjY2VzcylcbiAgICAgICAgICAgICAgICAgICAge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2NvbnRhY3Rtc2dcIikuaHRtbChkYXRhLmVycm9ycy5lbWFpbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAvKiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIG5hbWUgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjbmFtZS1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5uYW1lICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBlbWFpbCAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMuZW1haWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjZW1haWwtZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjZW1haWwtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLmVtYWlsICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBzdXBlcmhlcm8gYWxpYXMgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgIH0qL1xuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEFMTCBHT09EISBqdXN0IHNob3cgdGhlIHN1Y2Nlc3MgbWVzc2FnZSFcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICQoJ2Zvcm0nKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJhbGVydCBhbGVydC1zdWNjZXNzXCI+JyArIGRhdGEubWVzc2FnZSArICc8L2Rpdj4nKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdXN1YWxseSBhZnRlciBmb3JtIHN1Ym1pc3Npb24sIHlvdSdsbCB3YW50IHRvIHJlZGlyZWN0XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB3aW5kb3cubG9jYXRpb24gPSAnL3RoYW5rLXlvdSc7IC8vIHJlZGlyZWN0IGEgdXNlciB0byBhbm90aGVyIHBhZ2VcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vICBhbGVydCgnc3VjY2VzcycrZGF0YS5tZXNzYWdlKTsgLy8gZm9yIG5vdyB3ZSdsbCBqdXN0IGFsZXJ0IHRoZSB1c2VyXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjY29udGFjdG1zZ1wiKS5odG1sKGRhdGEubWVzc2FnZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gc3RvcCB0aGUgZm9ybSBmcm9tIHN1Ym1pdHRpbmcgdGhlIG5vcm1hbCB3YXkgYW5kIHJlZnJlc2hpbmcgdGhlIHBhZ2VcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB9KTtcblxuXG5cbiAgICAkKCcjZW5xdWlyeWZvcm0nKS5zdWJtaXQoZnVuY3Rpb24gKGV2ZW50KSB7XG5cbiAgICAgLy8gIHZhciByZXN1bHQgPSAkKCcjZW5xaWQnKS52YWwoKTtcbiAgICAgICB2YXIgbmFtZSA9ICQoJyNuYW1lJykudmFsKCk7XG4gICAgICAgdmFyIGVtYWlsPSQoJyNlbWFpbCcpLnZhbCgpO1xuICAgICAgIHZhciBtb2JpbGU9JCgnI21vYmlsZScpLnZhbCgpO1xuICAgICAgIHZhciBjb21tZW50cz0kKCcjY29tbWVudHMnKS52YWwoKTtcbiAgICAgXG4gICAgICAgXG4gICAgICAgLy8gYWxlcnQoXCJoYWlcIik7XG5cbiAgICAgICAgLy8gZ2V0IHRoZSBmb3JtIGRhdGFcbiAgICAgICAgLy8gdGhlcmUgYXJlIG1hbnkgd2F5cyB0byBnZXQgdGhpcyBkYXRhIHVzaW5nIGpRdWVyeSAoeW91IGNhbiB1c2UgdGhlIGNsYXNzIG9yIGlkIGFsc28pXG4gICAgICAgIHZhciBmb3JtRGF0YSA9IHtcbiAgICAgICAgICAgICAnbmFtZSc6IG5hbWUsXG4gICAgICAgICAgICAgJ2VtYWlsJzogZW1haWwsXG4gICAgICAgICAgICdjb21tZW50cyc6IGNvbW1lbnRzLFxuICAgICAgICAgICAgICdtb2JpbGUnICA6bW9iaWxlXG4gICAgICAgIH07XG4gICAgICAgLy8gY29uc29sZS5sb2coXCJIZWxsbyA6OiBcIitmb3JtRGF0YSk7XG4gICAgICAgLy8gXG4gICAgICAgLy8gXG4gICAgICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAgICAgXG4gICAgICAgLy8gYWxlcnQoZm9ybURhdGEubmFtZStcIjo6XCIrZm9ybURhdGEuZW1haWwgK1wiOjpcIitmb3JtRGF0YS5tb2JpbGUrXCI6OlwiK2Zvcm1EYXRhLmNvbW1lbnRzKTtcbiAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgIHR5cGU6ICdQT1NUJywgLy8gZGVmaW5lIHRoZSB0eXBlIG9mIEhUVFAgdmVyYiB3ZSB3YW50IHRvIHVzZSAoUE9TVCBmb3Igb3VyIGZvcm0pXG4gICAgICAgICAgICB1cmw6ICcvY29udGFjdCcsIC8vIHRoZSB1cmwgd2hlcmUgd2Ugd2FudCB0byBQT1NUXG4gICAgICAgICAgICBkYXRhOiBmb3JtRGF0YSwgLy8gb3VyIGRhdGEgb2JqZWN0XG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLCAvLyB3aGF0IHR5cGUgb2YgZGF0YSBkbyB3ZSBleHBlY3QgYmFjayBmcm9tIHRoZSBzZXJ2ZXJcbiAgICAgICAgICAgIGVuY29kZTogdHJ1ZVxuICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC8vIHVzaW5nIHRoZSBkb25lIHByb21pc2UgY2FsbGJhY2tcbiAgICAgICAgICAgICAgICAuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGxvZyBkYXRhIHRvIHRoZSBjb25zb2xlIHNvIHdlIGNhbiBzZWVcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaGVyZSB3ZSB3aWxsIGhhbmRsZSBlcnJvcnMgYW5kIHZhbGlkYXRpb24gbWVzc2FnZXNcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkYXRhLnN1Y2Nlc3MpXG4gICAgICAgICAgICAgICAgICAgIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjb250YWN0bXNnXCIpLmh0bWwoZGF0YS5tZXNzYWdlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3IgbmFtZSAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI25hbWUtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLm5hbWUgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIGVtYWlsIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5lbWFpbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuZW1haWwgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIHN1cGVyaGVybyBhbGlhcyAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfSovXG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQUxMIEdPT0QhIGp1c3Qgc2hvdyB0aGUgc3VjY2VzcyBtZXNzYWdlIVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gJCgnZm9ybScpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LXN1Y2Nlc3NcIj4nICsgZGF0YS5tZXNzYWdlICsgJzwvZGl2PicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c3VhbGx5IGFmdGVyIGZvcm0gc3VibWlzc2lvbiwgeW91J2xsIHdhbnQgdG8gcmVkaXJlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbmRvdy5sb2NhdGlvbiA9ICcvdGhhbmsteW91JzsgLy8gcmVkaXJlY3QgYSB1c2VyIHRvIGFub3RoZXIgcGFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gIGFsZXJ0KCdzdWNjZXNzJytkYXRhLm1lc3NhZ2UpOyAvLyBmb3Igbm93IHdlJ2xsIGp1c3QgYWxlcnQgdGhlIHVzZXJcblxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjb250YWN0bXNnXCIpLmh0bWwoZGF0YS5tZXNzYWdlKTtcblxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAvLyBzdG9wIHRoZSBmb3JtIGZyb20gc3VibWl0dGluZyB0aGUgbm9ybWFsIHdheSBhbmQgcmVmcmVzaGluZyB0aGUgcGFnZVxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH0pO1xuXG5cblxufSk7XG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gIC8vIGdldCBsb2NhdGlvbiBmcm9tIHVzZXIncyBJUCBhZGRyZXNzXG4gIFxuICAgIHZhciBsYXRpdHVkZSA9IDExLjY3MDA7XG4gICAgdmFyIGxvbmdpdHVkZSA9IDc1Ljk1Nzg7XG4gICAgXG4gICAgXG4gICAgICAkKFwiI2xvY2F0aW9uXCIpLmh0bWwoXG4gICAgICBcIkxvY2F0aW9uIDogIFdheWFuYWQgXCJcbiAgICApO1xuICAgICBcbiAgICAgRGF0ZS5wcm90b3R5cGUueXl5eW1tZGQgPSBmdW5jdGlvbigpIHtcbiAgdmFyIHl5eXkgPSB0aGlzLmdldEZ1bGxZZWFyKCkudG9TdHJpbmcoKTtcbiAgdmFyIG1tID0gKHRoaXMuZ2V0TW9udGgoKSsxKS50b1N0cmluZygpOyAvLyBnZXRNb250aCgpIGlzIHplcm8tYmFzZWRcbiAgdmFyIGRkICA9IHRoaXMuZ2V0RGF0ZSgpLnRvU3RyaW5nKCk7XG4gIHJldHVybiB5eXl5ICsgXCIvXCIgKyAobW1bMV0/bW06XCIwXCIrbW1bMF0pICsgXCIvXCIgKyAoZGRbMV0/ZGQ6XCIwXCIrZGRbMF0pOyAvLyBwYWRkaW5nXG59O1xuXG52YXIgZGF0ZSA9IG5ldyBEYXRlKCk7XG5cbiAgJChcIiNkYXRlXCIpLmh0bWwoZGF0ZSAgICApO1xuIFxuICAgIFxuICAvLyBnZXQgd2VhdGhlciB1c2luZyBPcGVuV2VhdGhlck1hcCBBUElcbiAgICAkLmdldEpTT04oXG4gICAgICBcImh0dHBzOi8vY29ycy41YXBwcy5jb20vP3VyaT1odHRwOi8vYXBpLm9wZW53ZWF0aGVybWFwLm9yZy9kYXRhLzIuNS93ZWF0aGVyP2xhdD1cIiArXG4gICAgICAgIGxhdGl0dWRlICtcbiAgICAgICAgXCImbG9uPVwiICtcbiAgICAgICAgbG9uZ2l0dWRlICtcbiAgICAgICAgXCImdW5pdHM9bWV0cmljJkFQUElEPWMzZTAwYzg4NjA2OTVmZDYwOTZmZTMyODk2MDQyZWRhXCIsXG4gICAgICBmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgIHZhciB3aW5kU3BlZWRrbWggPSBNYXRoLnJvdW5kKGRhdGEud2luZC5zcGVlZCAqIDMuNik7XG4gICAgICAgIHZhciBDZWxzaXVzID0gTWF0aC5yb3VuZChkYXRhLm1haW4udGVtcCk7XG4gICAgICAgIHZhciBpY29uSWQgPSBkYXRhLndlYXRoZXJbMF0uaWNvbjtcbiAgICAgICAgdmFyIHdlYXRoZXJVUkwgPSBcImh0dHA6Ly9vcGVud2VhdGhlcm1hcC5vcmcvaW1nL3cvXCIgKyBpY29uSWQgKyBcIi5wbmdcIjtcblxuICAgICAgICB2YXIgaWNvbkltZyA9IFwiPGltZyBzcmM9J1wiICsgd2VhdGhlclVSTCArIFwiJz5cIjtcbiAgICAgICAvLyAkKFwiI3NreS1pbWFnZVwiKS5odG1sKGljb25JbWcpO1xuICAgICAgICAkKFwiI3dlYXRoZXItaWRcIikuaHRtbChcIlNraWVzOiBcIiArIGRhdGEud2VhdGhlclswXS5kZXNjcmlwdGlvbik7XG5cbiAgICAgICAgJChcIiN0ZW1wZXJhdHVyZVwiKS5odG1sKENlbHNpdXMpO1xuICAgICAgICAkKFwiI3RvRmFocmVuaGVpdFwiKS5jbGljayhmdW5jdGlvbigpIHtcbiAgICAgICAgICAkKFwiI3RlbXBlcmF0dXJlXCIpLmh0bWwoTWF0aC5yb3VuZCg5IC8gNSAqIENlbHNpdXMgKyAzMikpO1xuICAgICAgICAgICQoXCIjd2luZC1zcGVlZFwiKS5odG1sKE1hdGgucm91bmQod2luZFNwZWVka21oICogMC42MjEpICsgXCIgbXBoXCIpO1xuICAgICAgICB9KTtcbiAgICAgICAgJChcIiN0b0NlbHNpdXNcIikuY2xpY2soZnVuY3Rpb24oKSB7XG4gICAgICAgICAgJChcIiN0ZW1wZXJhdHVyZVwiKS5odG1sKENlbHNpdXMpO1xuICAgICAgICAgICQoXCIjd2luZC1zcGVlZFwiKS5odG1sKHdpbmRTcGVlZGttaCArIFwiIGttL2hyXCIpO1xuICAgICAgICB9KTtcblxuICAgICAgICAkKFwiI3dpbmQtc3BlZWRcIikuaHRtbCh3aW5kU3BlZWRrbWggKyBcIiBrbS9oXCIpO1xuICAgICAgICAkKFwiI2h1bWlkaXR5XCIpLmh0bWwoIGRhdGEubWFpbi5odW1pZGl0eSArIFwiICVcIik7XG4gICAgICAgIFxuICAgICAgfVxuICAgICk7XG5cbiAgLypcbiAgJC5nZXRKU09OKFwiaHR0cHM6Ly9pcGluZm8uaW9cIiwgZnVuY3Rpb24oaW5mbykge1xuICAgIHZhciBsb2NTdHJpbmcgPSBpbmZvLmxvYy5zcGxpdChcIiwgXCIpO1xuICAgIFxuICAgIHZhciBsYXRpdHVkZSA9IHBhcnNlRmxvYXQobG9jU3RyaW5nWzBdKTtcbiAgICB2YXIgbG9uZ2l0dWRlID0gcGFyc2VGbG9hdChsb2NTdHJpbmdbMV0pO1xuICAgICQoXCIjbG9jYXRpb25cIikuaHRtbChcbiAgICAgIFwiTG9jYXRpb246IFwiICsgaW5mby5jaXR5ICsgXCIsIFwiICsgaW5mby5yZWdpb24gKyBcIiwgXCIgKyBpbmZvLmNvdW50cnlcbiAgICApO1xuXG4gICAgLy8gZ2V0IHdlYXRoZXIgdXNpbmcgT3BlbldlYXRoZXJNYXAgQVBJXG4gICAgJC5nZXRKU09OKFxuICAgICAgXCJodHRwczovL2NvcnMuNWFwcHMuY29tLz91cmk9aHR0cDovL2FwaS5vcGVud2VhdGhlcm1hcC5vcmcvZGF0YS8yLjUvd2VhdGhlcj9sYXQ9XCIgK1xuICAgICAgICBsYXRpdHVkZSArXG4gICAgICAgIFwiJmxvbj1cIiArXG4gICAgICAgIGxvbmdpdHVkZSArXG4gICAgICAgIFwiJnVuaXRzPW1ldHJpYyZBUFBJRD1jM2UwMGM4ODYwNjk1ZmQ2MDk2ZmUzMjg5NjA0MmVkYVwiLFxuICAgICAgZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICB2YXIgd2luZFNwZWVka21oID0gTWF0aC5yb3VuZChkYXRhLndpbmQuc3BlZWQgKiAzLjYpO1xuICAgICAgICB2YXIgQ2Vsc2l1cyA9IE1hdGgucm91bmQoZGF0YS5tYWluLnRlbXApO1xuICAgICAgICB2YXIgaWNvbklkID0gZGF0YS53ZWF0aGVyWzBdLmljb247XG4gICAgICAgIHZhciB3ZWF0aGVyVVJMID0gXCJodHRwOi8vb3BlbndlYXRoZXJtYXAub3JnL2ltZy93L1wiICsgaWNvbklkICsgXCIucG5nXCI7XG5cbiAgICAgICAgdmFyIGljb25JbWcgPSBcIjxpbWcgc3JjPSdcIiArIHdlYXRoZXJVUkwgKyBcIic+XCI7XG4gICAgICAgICQoXCIjc2t5LWltYWdlXCIpLmh0bWwoaWNvbkltZyk7XG4gICAgICAgICQoXCIjd2VhdGhlci1pZFwiKS5odG1sKFwiU2tpZXM6IFwiICsgZGF0YS53ZWF0aGVyWzBdLmRlc2NyaXB0aW9uKTtcblxuICAgICAgICAkKFwiI3RlbXBlcmF0dXJlXCIpLmh0bWwoQ2Vsc2l1cyk7XG4gICAgICAgICQoXCIjdG9GYWhyZW5oZWl0XCIpLmNsaWNrKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICQoXCIjdGVtcGVyYXR1cmVcIikuaHRtbChNYXRoLnJvdW5kKDkgLyA1ICogQ2Vsc2l1cyArIDMyKSk7XG4gICAgICAgICAgJChcIiN3aW5kLXNwZWVkXCIpLmh0bWwoTWF0aC5yb3VuZCh3aW5kU3BlZWRrbWggKiAwLjYyMSkgKyBcIiBtcGhcIik7XG4gICAgICAgIH0pO1xuICAgICAgICAkKFwiI3RvQ2Vsc2l1c1wiKS5jbGljayhmdW5jdGlvbigpIHtcbiAgICAgICAgICAkKFwiI3RlbXBlcmF0dXJlXCIpLmh0bWwoQ2Vsc2l1cyk7XG4gICAgICAgICAgJChcIiN3aW5kLXNwZWVkXCIpLmh0bWwod2luZFNwZWVka21oICsgXCIga20vaHJcIik7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoXCIjd2luZC1zcGVlZFwiKS5odG1sKHdpbmRTcGVlZGttaCArIFwiIGttL2hcIik7XG4gICAgICAgICQoXCIjaHVtaWRpdHlcIikuaHRtbChcIkh1bWlkaXR5OiBcIiArIGRhdGEubWFpbi5odW1pZGl0eSArIFwiICVcIik7XG4gICAgICB9XG4gICAgKTtcbiAgfSk7Ki9cbn0pO1xuXG5cblxuXG5cblxuXG4iLCIgXG5cblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7IFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIFxuICAgIGhpZGVfY3ViZSh0cnVlKTtcbiAgXG4gIFxufSk7IFxuXG5mdW5jdGlvbiBoaWRlX2N1YmUoaGlkZSkge1xuICAgIGlmIChoaWRlID09IHRydWUpIHtcbiAgICAgICAgJChcIiN3YXlhbmFkXCIpLmNzcyhcInZpc2liaWxpdHlcIiwgXCJoaWRkZW5cIik7XG4gICAgIH0gZWxzZSB7XG4gICAgICAgICQoXCIjd2F5YW5hZFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICAgIH1cbn0gXG5cblxuLypcbiAkKHdpbmRvdykubG9hZChmdW5jdGlvbigpIHsgXG4gICAgIC8vaW5zZXJ0IGFsbCB5b3VyIGFqYXggY2FsbGJhY2sgY29kZSBoZXJlLiBcbiAgICAgLy9XaGljaCB3aWxsIHJ1biBvbmx5IGFmdGVyIHBhZ2UgaXMgZnVsbHkgbG9hZGVkIGluIGJhY2tncm91bmQuXG4gICAgIC8vIFxuICB9KTtcbiBcbiovXG5cbiQod2luZG93KS5vbihcImxvYWRcIiwgZnVuY3Rpb24oKSB7XG4gIC8vIFRoaW5ncyB0aGF0IG5lZWQgdG8gaGFwcGVuIGFmdGVyIGZ1bGwgbG9hZFxuICAgaGlkZV9jdWJlKGZhbHNlKTtcbn0pO1xuXG5cblxuIiwiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKClcbntcbiAgICAkKFwiI2J1dHRvbl9wa2dcIikuY2xpY2soZnVuY3Rpb24oZSl7XG4gIC8vICBhbGVydChcIlRoZSBwYXJhZ3JhcGggd2FzIGNsaWNrZWQuXCIpO1xuICAgIFxuICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAvL3Jlc2V0IGFuaW1hdGlvblxuICAgICAgICBlLnRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKCdhbmltYXRlJyk7XG5cbiAgICAgICAgZS50YXJnZXQuY2xhc3NMaXN0LmFkZCgnYW5pbWF0ZScpO1xuICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBlLnRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKCdhbmltYXRlJyk7XG4gICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBcIi9wYWNrYWdlXCI7XG4gICAgICAgIH0sIDcwMCk7XG4gICBcbn0pO1xuXG5cbiAgICB2YXIgYW5pbWF0ZUJ1dHRvbiA9IGZ1bmN0aW9uIChlKSB7XG5cbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAvL3Jlc2V0IGFuaW1hdGlvblxuICAgICAgICBlLnRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKCdhbmltYXRlJyk7XG5cbiAgICAgICAgZS50YXJnZXQuY2xhc3NMaXN0LmFkZCgnYW5pbWF0ZScpO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGUudGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoJ2FuaW1hdGUnKTtcbiAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IFwiL3BhY2thZ2VcIjtcbiAgICAgICAgfSwgNzAwKTtcbiAgICB9O1xuLypcbiAgICB2YXIgYnViYmx5QnV0dG9ucyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoXCJidWJibHktYnV0dG9uXCIpO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBidWJibHlCdXR0b25zLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGJ1YmJseUJ1dHRvbnNbaV0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBhbmltYXRlQnV0dG9uLCBmYWxzZSk7XG4gICAgfVxuKi9cblxufSk7XG5cblxuIl19
