@extends('aa_ServerPart.aa_WorkSpace.emails.base-email')

@section('body')
<br>

<br>

<br>

{{--    
<h4> Details <h4>
    <strong> Name   <strong> ,{!! $name !!}<br>
    <strong> Mobile <strong> ,{!! $mobile !!}<br>          
    <strong> Email  <strong> ,{!! $email !!} <br>
                    <h4> Message <h4>
                      {!! $message !!}<br>
                       
</p>
--}}

<style>
            
@import url(https://fonts.googleapis.com/css?family=Tangerine);

@import url(https://fonts.googleapis.com/css?family=Tangerine);

@import url(https://fonts.googleapis.com/css?family=Josefin+Sans:300i|PT+Serif);

@import url(https://fonts.googleapis.com/css?family=Abril+Fatface);

table {
    width:100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color:#fff;
}
table#t01 th {
    background-color: black;
    color: white;
}

.company_caption_font
{
  font-family: "Tangerine", cursive;
  text-shadow: 4px 4px 4px #aaa;
  
}

.company_moto_font
{
  font-family: "Abril Fatface", cursive;
  color: black;
}
</style>

<table id="t01" >
  <tr>
    <th>Name</th>
    <td>{!! $name !!}</td>
  </tr>
  <tr>
    <th>Mobile</th>
    <td>{!! $mobile !!}</td>
  </tr>
  <tr>
    <th>Email</th>
    <td>{!! $email !!}</td>
  </tr>
 
  <tr>
    <th>Package</th>
    <td>{!! $package !!}</td>
  </tr>

  <tr>
    <th>Duration</th>
    <td>{!! $duration !!}</td>
  </tr>

  <tr>
    <th>Start Date</th>
    <td>{!! $date_from !!}</td>
  </tr>
  
  <tr>
    <th>End Date</th>
    <td>{!! $date_to !!}</td>
  </tr>
 
  <tr>
    <th>Number of People</th>
    <td>{!! $number_of_person !!}</td>
  </tr>
  
</table>

                
   
 
  
@stop
