<?php $__env->startSection('image_pkg'); ?> 

 

 <div class="card-deck">

<?php $__currentLoopData = $galleryPackage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $gallery_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


 <?php
   $classbg = "cardSet".$index%3;
 ?>


               <?php if($index%2==0 && $index!=0): ?>
                <div class="d-none d-md-block d-sm-block d-lg-none  w-100" style="height:25px"><!-- wrap every 2 on sm--></div>



                <?php endif; ?>

                <?php if($index%3==0 && $index!=0 ): ?>
                 <div class="d-none d-lg-block w-100" style="height:25px"><!-- wrap every 2 on sm--></div>
                <?php endif; ?>

      
                   <div class="card  <?php echo $classbg; ?>">
                    <img class="card-img-top" src= "<?php echo $gallery_item->image_link; ?>" alt="Card image cap">
            <?php if( $gallery_item->tentative==1): ?>
                    <div class="card-header text-center display-5 text-white" style="font-size: 23px;  background-color:  #00b300;text-shadow: 2px 2px 4px #000000;
" ><?php echo $gallery_item->heading; ?></div>

            <?php else: ?>
                    <div class="card-header text-center display-5 text-white" style="font-size: 25px;  background-color:  #00b300;text-shadow: 2px 2px 4px #000000;
" ><?php echo $gallery_item->heading; ?></div>
            
            <?php endif; ?>

                    <div class="card-content-body  text-white">
                        <div class="card-block">
        
                       <?php if( $gallery_item->tentative==1): ?>
                           <div class="text-center mb-3"><a class="card-link btn btn-primary">Cost Tentative</a></div>
                       <?php else: ?>
                           <h3 class="card-title text-center display-6"><?php echo $gallery_item->subheading; ?></h3>
                           <div class="text-center mb-3"><a class="card-link btn btn-primary"><span>&#8377;</span> <?php echo $gallery_item->cost; ?> <?php echo $gallery_item->payment_mode; ?> </a></div>
                            
                       <?php endif; ?>


                        <div class="scroll_mobile">
                                    <?php if(strlen( $gallery_item->day1 )!=0): ?>

                <div class="card-text text-justify">

                    <?php echo $gallery_item->day1; ?>  
                </div> 
                <?php endif; ?>

                <?php if(strlen( $gallery_item->day2 )!=0): ?>

                <div class="card-text  text-justify ">

                    <?php echo $gallery_item->day2; ?>  
                </div> 
                <?php endif; ?>


                <?php if(strlen( $gallery_item->day3 )!=0): ?>

                <div class="card-text text-justify ">

                    <?php echo $gallery_item->day3; ?>  
                </div> 
                <?php endif; ?>

                <?php if(strlen( $gallery_item->day4 )!=0): ?>

                <div class="card-text text-justify ">

                    <?php echo $gallery_item->day4; ?>  
                </div> 
                <?php endif; ?>


                <?php if(strlen( $gallery_item->day5 )!=0): ?>

                <div class="card-text text-justify ">

                    <?php echo $gallery_item->day5; ?>  
                </div> 
                <?php endif; ?>
      
                       </div>
                        </div>

                    </div>
            
                 <form name="bookform" id="loginforml" action="/booking-from_list" method="post" style="margin: 10px;float: bottom">
                  
                  <input type="hidden" name="package" value="<?php echo $gallery_item->heading; ?>">
 
                  <input type="hidden" name="duration" value="<?php echo $gallery_item->subheading; ?>">
                     
                    <div class="text-center">
                         <button style="float:center" class="roundB btn-primary">Book</button>
                    </div>    

                </form>               

                </div> 


                 
 

 
 

                


 
 

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>

<?php $__env->stopSection(); ?> 



<div id="package" >
    <h2 class= "text-center"> <span class="font_package_heading"> Packages</span></h2><br>
    <br> 
    <div class="container">
       



            <?php echo $__env->yieldContent('image_pkg'); ?>  


      




    </div>

    <br> 
    <br> 
    <br> 





</div>

