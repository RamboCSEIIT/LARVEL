 
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQqlQMknlW6SbS9ghtqTW72cYvV8yZT-w"></script>
 


 <script src="03_Slicebox/js/jquery.slicebox.min.js"></script>
  <script>
    $(function() {
      var Page = (function() {
        var $navArrows = $('#nav-arrows').hide(),
          $navDots = $('#nav-dots').hide(),
          $nav = $navDots.children('span'),
          $shadow = $('#shadow').hide(),
          slicebox = $('#sb-slider').slicebox( {
            onReady: function() {
              $navArrows.show();
              $navDots.show();
              $shadow.show();
            },
            onBeforeChange: function(pos) {
              $nav.removeClass('nav-dot-current');
              $nav.eq(pos).addClass('nav-dot-current');
            }
          } ),
          init = function() {
            initEvents();
          },
          initEvents = function() {
            $navArrows.children(':first').on('click', function() {
              slicebox.next();
              return false;
            } );
            $navArrows.children(':last').on('click', function() {
              slicebox.previous();
              return false;
            } );
            $nav.each( function(i) {
              $(this).on('click', function(event) {
                var $dot = $(this);
                if(!slicebox.isActive()) {
                  $nav.removeClass('nav-dot-current');
                  $dot.addClass('nav-dot-current');
                }
                slicebox.jump(i+1);
                return false;
              });
            });
          };
          return { init : init };
      })();
      Page.init();
    });
  </script>

<script type="text/javascript" src="/01_SCRIPTS/aa_home_page.js"></script> 


