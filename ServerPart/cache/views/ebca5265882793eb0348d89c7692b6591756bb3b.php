<?php $__env->startSection('banasura'); ?>

<div class="container-fluid" id="tour-places" style="background: graytext;" >
    

 
  <div  class="card_model card_v">
         <div class="container">
        <div class="front" style="background-image: /02_IMAGES/af_TOUR_PLACES_PAGE/a_banasura.jpg">
          <div class="inner">
              
          </div>
                                       <div class="text-center mb-3"><a class="card-link btn btn-primary">Price Details</a></div>

        </div>
        <div class="back">
            <div class="inner" >
               
           </div>
            
            
         </div>
      </div>

    </div>



    <!-- end container -->
     
</div>

<?php $__env->stopSection(); ?>


<div class="reveal">
    <div class="slides">
        <section>
                 <?php echo $__env->yieldContent('banasura'); ?>
        </section>
        
        <section>
            <h1>Working at<br><img src="images/mobile.de-logo.png"></h1>
            <p>
                Patrick Hund<br>
                Senior Software Engineer<br>
                <a href="mailto:pahund@team.mobile.de">pahund@team.mobile.de</a>
            </p>
        </section>       
        
     </div>
</div>

