<?php $__env->startSection('body'); ?>
<br>

<br>

<br>



<style>
            
@import  url(https://fonts.googleapis.com/css?family=Tangerine);

@import  url(https://fonts.googleapis.com/css?family=Tangerine);

@import  url(https://fonts.googleapis.com/css?family=Josefin+Sans:300i|PT+Serif);

@import  url(https://fonts.googleapis.com/css?family=Abril+Fatface);

table {
    width:100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color:#fff;
}
table#t01 th {
    background-color: black;
    color: white;
}

.company_caption_font
{
  font-family: "Tangerine", cursive;
  text-shadow: 4px 4px 4px #aaa;
  
}

.company_moto_font
{
  font-family: "Abril Fatface", cursive;
  color: black;
}
</style>

<table id="t01" >
  <tr>
    <th>Name</th>
    <td><?php echo $name; ?></td>
  </tr>
  <tr>
    <th>Mobile</th>
    <td><?php echo $mobile; ?></td>
  </tr>
  <tr>
    <th>Email</th>
    <td><?php echo $email; ?></td>
  </tr>
 
  <tr>
    <th>Package</th>
    <td><?php echo $package; ?></td>
  </tr>

  <tr>
    <th>Duration</th>
    <td><?php echo $duration; ?></td>
  </tr>

  <tr>
    <th>Start Date</th>
    <td><?php echo $date_from; ?></td>
  </tr>
  
  <tr>
    <th>End Date</th>
    <td><?php echo $date_to; ?></td>
  </tr>
 
  <tr>
    <th>Number of People</th>
    <td><?php echo $number_of_person; ?></td>
  </tr>
  
</table>

                
   
 
  
<?php $__env->stopSection(); ?>

<?php echo $__env->make('aa_ServerPart.aa_WorkSpace.emails.base-email', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>