<?php  namespace tour\Controllers;
//blade ps2 == twig not
use duncan3dc\Laravel\BladeInstance;
use Kunststube\CSRFP\SignatureGenerator;

class BaseController  
{
 

   protected $blade;
   protected $signer; 
   protected $signerT; 
 
    function __construct()
    {
        $this->blade = new BladeInstance(getenv('VIEWS_DIRECTORY'), getenv('CACHE_DIRECTORY'));
        $this->signer = new SignatureGenerator(getenv('CSRF_SECRET'));
        $this->signerT = new SignatureGenerator(getenv('CSRF_SECRET_T'));

    }
    
    function trimblanklines($str) 
    {
         return preg_replace('`\A[ \t]*\r?\n|\r?\n[ \t]*\Z`','',$str);
    }
  
} 
?>
