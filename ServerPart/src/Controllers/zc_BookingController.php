<?php namespace tour\Controllers;
use tour\Validation\Validator;
//authors decided file level directives
use duncan3dc\Laravel\BladeInstance;
use Illuminate\Database\Capsule\Manager as DB;
use tour\email_send\Semail;
use tour\auth\LoggedIn;

class zc_BookingController extends BaseController {
    
    
   
    public function getShowBookingPage() 
    {
        /*
       if(!(isset($_SESSION['package']) && isset($_SESSION['package']))) 
       {
                      header("Location: /page-not-found");
           exit();
       }

        $galleryPackage = DB::select('SELECT * FROM galleryPackage');

 

        
       // dd("admin");
       
       $code = $this->blade->render('aa_ServerPart.aa_WorkSpace.ag_BOOKING_PAGE.booking_page', [
            'page_name' => '#BookingPage',
            'package' => "package",
            'duration'=>"duration"
           
         ]);





        echo $code;
 


*/

       // echo $code;
    }
    
    
    public function postShowBookingPage() 
    {
        $test=[];
        $test[0]=$package=$_REQUEST['package'];
        $test[1]=$duration=$_REQUEST['duration'];
        $test[2]=$date_from=$_REQUEST['from'];
        $test[3]=$date_to=$_REQUEST['to'];
        $test[4]=$number_of_person=$_REQUEST['number_of_person'];

        
        
        
     
   //     dd($test); 
        
        
        if(!LoggedIn::user())
        {    
           header("Location: /login-before-booking");
           exit();
        }
       
  $Name = LoggedIn::user()[0]->first_name;
       $Email=LoggedIn::user()[0]->email;
       $Mobile=LoggedIn::user()[0]->mob_number;
    

        $message_to_admin = $this->blade->render('aa_ServerPart.aa_WorkSpace.emails.booking-email-to-admin',
            [  'name' => $Name ,
               'mobile' =>$Mobile,
                'email' => $Email,
                'package'=>$package,
                'duration'=>$duration,
                'date_from'=>$date_from,
                'date_to'  => $date_to,
                'number_of_person'=>$number_of_person
               
               
             ]
        );
        
        $message_to_customer = $this->blade->render('aa_ServerPart.aa_WorkSpace.emails.booking-email-to-customer' );

               
   
        Semail::_semail(getenv('ENQ_DEST_HOST'), "new Booking", $message_to_admin);
        Semail::_semail($Email, "Booking Banasura Hill Valley Home Stay", $message_to_customer);

   
         header("Location: /success-booking");
         
          unset($_REQUEST['package']);
          unset($_REQUEST['duration']);

           exit();
  
       
       // echo $code;
    }
    
  
    
    
    public function postShowBookingPageList() 
    {
        $test=[];
        $test[0]=$package=$_REQUEST['package'];
        $test[1]=$duration=$_REQUEST['duration'];
                
           $code = $this->blade->render('aa_ServerPart.aa_WorkSpace.ag_BOOKING_PAGE.booking_page', [
            'page_name' => '#BookingPage',
            'package' => $_REQUEST['package'],
            'duration'=>$_REQUEST['duration']
           
         ]);


        


        echo $code;
              unset($_POST['package']);
          unset($_POST['duration']);

        exit();
 
        
       
       
       
       // echo $code;
    }
    
  
    
    
    
}

?>
