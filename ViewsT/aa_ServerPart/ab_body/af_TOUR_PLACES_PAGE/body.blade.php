@section('banasura')

<div class="container-fluid" id="tour-places" style="background: graytext;" >
    
{{--
    <div class="container " style="width:100%;padding: 0px;border: 0px;margin: 30px 0px;overflow: hidden" id="image_holder">


        <div class="row justify-content-center text-center" style="height:inherit">
            <div class="col-12"> 

                <img class="img-fluid" src="/02_IMAGES/af_TOUR_PLACES_PAGE/a_banasura.jpg" >
                <div class="title1">Banasura Dam</div>



                <!-- end details -->


                <!-- end hero -->



            </div>

        </div>
    </div>


--}}
 
  <div  class="card_model card_v">
         <div class="container">
        <div class="front" style="background-image: /02_IMAGES/af_TOUR_PLACES_PAGE/a_banasura.jpg">
          <div class="inner">
              
          </div>
                                       <div class="text-center mb-3"><a class="card-link btn btn-primary">Price Details</a></div>

        </div>
        <div class="back">
            <div class="inner" >
               
           </div>
            
            
         </div>
      </div>

    </div>


{{--

<div class="row">
    <div class="col-12 text-justify" style="">
                  <img class="img-fluid" src="/02_IMAGES/af_TOUR_PLACES_PAGE/a_banasura.jpg" >


    </div>
</div>

<div class="row">
    <div class="col-12 text-justify" style="font-size:20px;">
           <p>         

            Another tourist attraction of Kalpetta is Banasura Sagar dam. It is the largest earth dam in India and the second largest of its kind in Asia. The dam is made up of massive stacks of stones and boulders. Situated about 15 km from Kalpetta, the dam holds a large expanse of water and its picturesque beauty is enhanced by the chain of mountains seen on the backdrop.  It is constructed in the Banasura Lake and the nearby mountains are known as Banasura Hills. Legends say that the Asura king of Banasura, (the son of King Mahabali, who is believed to visit Kerala during every Onam festival) undertook a severe penance on the top of these hills and thus it was named after him.  The scenic mountains beckon adventure tourists and the dam site is an ideal starting point for trekking. 
            <br>
            During monsoon, visitors may also be able to see small islands in the dam’s reservoir. They are formed while the flooded reservoir submerges the surrounding areas too.  The vast expanse of the crystal clear water of the dam dotted with small islands is a photographer’s delight.



        </p>


    </div>
</div>
--}}
    <!-- end container -->
     
</div>

@stop


<div class="reveal">
    <div class="slides">
        <section>
                 @yield('banasura')
        </section>
        
        <section>
            <h1>Working at<br><img src="images/mobile.de-logo.png"></h1>
            <p>
                Patrick Hund<br>
                Senior Software Engineer<br>
                <a href="mailto:pahund@team.mobile.de">pahund@team.mobile.de</a>
            </p>
        </section>       
        
     </div>
</div>

